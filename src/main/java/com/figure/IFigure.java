package com.figure;

public interface IFigure {

    void erase(int x, int y);

    void draw(int x, int y);

}
